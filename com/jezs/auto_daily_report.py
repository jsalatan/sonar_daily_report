'''
Created on Feb 3, 2013

@author: Jonathan
'''
import auth
import mailer
import report
import sys

if __name__ == '__main__':
  # Create a connection
  cookie = auth.connect_using_httplib2_cookies()
  
  # Pass cookies/session to report module for report generation
  project_id = sys.argv[1]
  duration = sys.argv[2]
  final_report = report.get_final_report(cookie, project_id, duration, False)
#  final_report = report.get_dummy_tracker_report()
  
  # Pass report as email body
  mailer.send_mail(final_report, sys.argv[3], "Daily")