'''
Created on Feb 3, 2013

@author: Jonathan
'''
from httplib import HTTPConnection
import httplib2
import urllib
import logging

logger = logging.getLogger('auth')
report_url = "http://sonar.dev.novare.com.hk:9000/dashboard/index/1?did=1&period=1"
login_url = "http://sonar.dev.novare.com.hk:9000/sessions/login"

def connect_using_httplib2_cookies():
  url = login_url  
  body = {'login': 'username', 'password': 'password'}
  headers = {'Content-type': 'application/x-www-form-urlencoded'}

  http = httplib2.Http(disable_ssl_certificate_validation=True)
  response, content = http.request(url, 'POST', headers=headers, body=urllib.urlencode(body))

  return response['set-cookie']

if __name__ == '__main__':
  HTTPConnection.debuglevel = 1
#  connect_using_urllib2()
#  connect_using_requests()
#  connect_using_httplib2()
  cookie = connect_using_httplib2_cookies()
  print cookie
#  report_url = "https://redmine.dev.novare.com.hk/redmine/projects/psb-aas-v61/issues/report/priority"

#  response = urllib2.urlopen(report_url)
#  print response.read()  
