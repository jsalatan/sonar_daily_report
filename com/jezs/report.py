'''
Created on Feb 3, 2013

@author: Jonathan
'''
from bs4 import BeautifulSoup
import report_parser
import httplib2
import premailer
import urllib

project_url = "http://sonar.dev.novare.com.hk:9000/dashboard/index/%s?"
dashboard_id = "1"
hotspots_id = "2"
issues_id = "3"
issues_drilldowns_url = "http://sonar.dev.novare.com.hk:9000/drilldown/issues/%s?"
period_since_previous = "1"
period_over_thirty = "2"
period_prev_version = "3"

'''
1. Get HTML content
2. Parse HTML content to extract only information needed for email
3. Apply style
4. Return report
'''
def get_final_report(cookie, project_id, period, is_weekly):
  # Get HTML content
  dashboard_html = get_html("dashboard", period, cookie, project_id)
  drilldown_html = get_html("drilldown", period, cookie, project_id)

  # Extract report tables
  dashboard_report_table = report_parser.generate_summary_table(dashboard_html)
  drilldown_table = report_parser.generate_drilldown_table(drilldown_html)
  
  msg = "the result of Sonar's code analysis"
  if (is_weekly == False):
    issues_html = get_html("issues", period, cookie, project_id)
    unresolved_per_assignee_table = report_parser.generate_unresolved_per_assignee_table(issues_html)
    final_report = generate_report(msg, dashboard_report_table, drilldown_table, unresolved_per_assignee_table)
  else:
    final_report = generate_report(msg, dashboard_report_table, drilldown_table, "N/A")
  return final_report

def get_html(report_type, period, cookie, project_id):
  report_url = ""
  if (report_type == "dashboard"):
    params = urllib.urlencode({"period": period, "did": dashboard_id})
    report_url = (project_url % project_id) + params
  elif (report_type == "hotspots"):
    params = urllib.urlencode({"period": period, "did": hotspots_id})
    report_url = (project_url % project_id) + params
  elif (report_type == "issues"):
    params = urllib.urlencode({"period": period, "did": issues_id})
    report_url = (project_url % project_id) + params
  elif (report_type == "drilldown"):
    params = urllib.urlencode({"period": period})
    report_url = (issues_drilldowns_url % project_id) + params

  http = httplib2.Http(disable_ssl_certificate_validation=True)
  headers = {'Cookie': cookie}
  response, content = http.request(report_url, 'GET', headers=headers)
  return content

def generate_report(msg, dashboard_report, drilldown_report, unresolved_issues):
  html_report = """
  <html>
  <head>
  <style type='text/css'>
    table.list { border: 1px solid #e4e4e4;  border-collapse: collapse; width: 100%%; margin-bottom: 4px; }
    table.list th {  background-color:#EEEEEE; padding: 4px; white-space:nowrap; }
    table.list td { vertical-align: top; }
    table.list td.id { width: 2%%; text-align: center;}
    table.list caption { text-align: left; padding: 0.5em 0.5em 0.5em 0; }
    table.list tbody tr:hover { background-color:#ffffdd; }
    table.list tbody tr.group:hover { background-color:inherit; }
    table td {padding:2px;}
    table p {margin:0;}
    .odd {background-color:#f6f7f8;}
    .even {background-color: #fff;}
    table.attributes { width: 100%% }
    table.attributes th { vertical-align: top; text-align: left; }
    table.attributes td { vertical-align: top; }
  </style>
  </head>
  <body>
    <div>
      <p>Good day,<p>
      <p>Please see tables below for %s.<p>
    </div>
    <h3>Dashboard</h3>
    %s
    <h3>Issues Drilldown</h3>
    %s
    <h3>Unresolved Issues per Assignee</h3>
    %s
    <div>
      <br/>
      <p>Best Regards,</p>
      <pre style="color:#000000; font-size:10pt; font-family:'Courier New';"><span style="color:#ff0080; font-weight:bold">--</span> 
      
      Jonathan Salatan
      Senior Software Engineer
      
      Novare Technologies
      <span style="color:#800080; font-weight:bold">7F</span> Peninsula Court Building
      <span style="color:#800080; font-weight:bold">8735</span> Paseo De Roxas corner Makati Avenue
      Makati City <span style="color:#800080; font-weight:bold">1226</span> Philippines
      
      Email <span style="color:#8080c0; font-weight:bold">Address</span><span style="color:#ff0080; font-weight:bold">:</span> jonathan<span style="color:#ff0080; font-weight:bold">.</span>salatan&#64;novare<span style="color:#ff0080; font-weight:bold">.</span>com<span style="color:#ff0080; font-weight:bold">.</span>hk
      Mobile No<span style="color:#ff0080; font-weight:bold">.: +</span><span style="color:#800080; font-weight:bold">63 917 8306311</span>
      Office <span style="color:#bb7977; font-weight:bold">Phone</span><span style="color:#ff0080; font-weight:bold">: +</span><span style="color:#800080; font-weight:bold">632 324 0001</span> <span style="color:#ff0080; font-weight:bold">(</span>local <span style="color:#800080; font-weight:bold">566</span><span style="color:#ff0080; font-weight:bold">)</span>
      www<span style="color:#ff0080; font-weight:bold">.</span>novare<span style="color:#ff0080; font-weight:bold">.</span>com<span style="color:#ff0080; font-weight:bold">.</span>hk
      </pre>
    </div>
  </body>
  </html>""" % (msg, dashboard_report, drilldown_report, unresolved_issues)

  return premailer.transform(html_report)

def get_dummy_tracker_report():
  dashboard_html = open('C:\\Users\\Jonathan\\Desktop\\sonar_report\\psbaas_core.htm').read()
  drilldown_html = open('C:\\Users\\Jonathan\\Desktop\\sonar_report\\issues_drilldown.htm').read()

  # Extract report tables
  dashboard_report_table = report_parser.generate_summary_table(dashboard_html)
  drilldown_report_table = report_parser.generate_drilldown_table(drilldown_html)

  msg = "the result of Sonar's code analysis."
  final_report = generate_report(msg, dashboard_report_table, drilldown_report_table)
  return final_report

def pre_mailer(final_report):
#  http = httplib2.Http(disable_ssl_certificate_validation=True)
#  headers = {'Cookie': cookie}
  soup = BeautifulSoup(final_report)
  stylesheets = soup.findAll("link", {"rel": "stylesheet"})
  for s in stylesheets:
#    remote_css = http.get
    s.replaceWith('<style type="text/css" media="screen">' + open(s["href"]).read() + '</style>')
  return str(soup)

if __name__ == '__main__':
  print (get_dummy_tracker_report())