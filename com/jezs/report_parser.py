'''
Created on Feb 3, 2013

@author: Jonathan
'''
from bs4 import BeautifulSoup
from bs4.element import NavigableString
import re

class ReportParser:
  def __init__(self):
    pass

def parse_table(html):
  soup = strip_tags(html, ['a'])
  table = soup.table
  return table

def strip_tags(html, invalid_tags):
    soup = BeautifulSoup(html)
    for tag in soup.findAll(True):
        if tag.name in invalid_tags:
            s = ""
            for c in tag.contents:
                if not isinstance(c, NavigableString):
                    c = strip_tags(unicode(c), invalid_tags)
                s += unicode(c)
            tag.replaceWith(s)
    return soup

summary_row_labels = ["Rules compliance", "Quality Index", "Total Quality", "Added", "Removed"]

'''
1. Find target module
2. Find module % done
'''
def generate_summary_table(html):
  data_dict = parse_summary_values(html)
  values_rows = generate_values_rows(summary_row_labels, data_dict)
  table = """
  <table style="border-collapse:collapse; margin-bottom:4px; border:1px solid #e4e4e4;">
    %s
  </table>""" % values_rows
  return table

def parse_summary_values(html):
  values_dict = {}
  soup = BeautifulSoup(html)

  # Soup for 'Added' and 'Removed'
  spanList = soup.find("div", {"id": "block_7"}).find("div", {"class": "dashbox"}).find("h3").findNextSibling('div').findAll('span', {"style": 'font-weight: bold'})

  # 'Added'
  label = summary_row_labels[3]
  print(len(spanList))
  if (not spanList or len(spanList) < 0):
    valueStr = "0"
  else:
    valueStr = spanList[0].a.string
  values_dict[label] = valueStr

  # 'Removed'
  label = summary_row_labels[4]
  if (not spanList or len(spanList) <= 1):
    valueStr = "0"
  else:
    valueStr = spanList[1].span.string
  values_dict[label] = valueStr

  # Soup for 'Rules Compliance'
  rulesCompDiv = soup.find("div", {"id": "block_7"}).find("div", {"class": "dashbox"}).find("h3", text="Rules compliance").findNextSibling('div')

  # 'Rules Compliance'
  label = summary_row_labels[0]
  percent = rulesCompDiv.find('span', {"id": 'm_violations_density'}).string
  variance = rulesCompDiv.findAll('span')[2].string

  valueStr = percent + variance
  values_dict[label] = valueStr

  # 'Quality Index'
  label = summary_row_labels[1]
  valueStr = soup.find('span', {"id": 'm_qi-quality-index'}).string

  values_dict[label] = valueStr

  # 'Total Quality'
  label = summary_row_labels[2]
  valueStr = soup.find('span', {"id": 'm_total-quality'}).string

  values_dict[label] = valueStr

  return values_dict

drilldown_row_labels = ["Blocker", "Critical", "Major", "Minor", "Info"]

def generate_drilldown_table(html):
  #Parse drilldown values
  data_dict = parse_drilldown_values(html)
  values_rows = generate_values_rows(drilldown_row_labels, data_dict)
  table = """
  <table style="border-collapse:collapse; margin-bottom:4px; border:1px solid #e4e4e4">
    %s
  </table>""" % values_rows
  return table

def parse_drilldown_values(html):
  # Parse drilldown values
  values_dict = {}
  soup = BeautifulSoup(html)

  # 'Blocker'
  label = drilldown_row_labels[0]
  valueStr = soup.find("table", {"class": "spacedicon"}).findAll("span")[0].string
  values_dict[label] = valueStr

  # 'Critical'
  label = drilldown_row_labels[1]
  valueStr = soup.find("table", {"class": "spacedicon"}).findAll("span")[1].string
  values_dict[label] = valueStr

  # 'Major'
  label = drilldown_row_labels[2]
  valueStr = soup.find("table", {"class": "spacedicon"}).findAll("span")[2].string
  values_dict[label] = valueStr

  # 'Minor'
  label = drilldown_row_labels[3]
  valueStr = soup.find("table", {"class": "spacedicon"}).findAll("span")[3].string
  values_dict[label] = valueStr

  # 'Info'
  label = drilldown_row_labels[4]
  valueStr = soup.find("table", {"class": "spacedicon"}).findAll("span")[4].string
  values_dict[label] = valueStr

  return values_dict

def generate_unresolved_per_assignee_table(html):
  #Parse unresolved values
  data_dict, assignees = parse_unresolved_per_assignee_values(html)
  values_rows = generate_values_rows(assignees, data_dict)
  table = """
  <table style="border-collapse:collapse; margin-bottom:4px; border:1px solid #e4e4e4">
    %s
  </table>""" % values_rows
  return table

def parse_unresolved_per_assignee_values(html):
  # Parse values
  values_dict = {}
  assignees = []
  soup = BeautifulSoup(html)

  # Get widget
  tableRows = soup.find("div", {"id": "unresolved-issues-per-assignee-widget-22"}).find("table").find("tbody").findAll("tr")

  for row in tableRows:
    assignee = row.findAll("td")[0].a.string;
    issue_count = row.findAll("td")[1];

    values_dict[assignee] = issue_count
    assignees.append(assignee) 

  return values_dict, assignees

style_odd = 'style="{background-color:#f6f7f8} :hover{background-color:#ffffdd}" bgcolor="#f6f7f8"'
style_even = 'style="{background-color:#fff} :hover{background-color:#ffffdd}" bgcolor="#fff"'

def generate_values_rows(row_labels, data_dict):
  row = """
  <tr %s>
    <td style="padding:2px; vertical-align:top">%s</td>
    <td align="center" style="padding:2px; vertical-align:top"><strong>%s</strong></td>
  </tr>"""
  values_rows = ""
  for index, label in enumerate(row_labels):
    style = style_even if index % 2 == 0 else style_odd
    value = data_dict[label]
    values_rows = values_rows + row % (style, label, value)

  return values_rows

if __name__ == '__main__':
#   report = open('C:\\Users\\Jonathan\\Desktop\\sonar_report\\issues_drilldown2.htm')
#   print(generate_unresolved_per_assignee_table(report.read()))

  report = open('C:\\Users\\Jonathan\\Desktop\\sonar_report\\psbaas_core.htm')
  print(generate_summary_table(report.read()))
